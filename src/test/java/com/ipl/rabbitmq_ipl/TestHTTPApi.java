package com.ipl.rabbitmq_ipl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.codec.binary.Base64;

import com.ipl.config.RabbitMQConfig;
import com.ipl.queueInfo.QueueInfo;
import com.ipl.queueInfo.QueueInfoList;
import com.ipl.util.gson.GsonUtil;

public class TestHTTPApi {

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) throws IOException {
		RabbitMQConfig cfg = ConfigFactory.create(RabbitMQConfig.class);
		String url = "http://" + cfg.hostname() + ":" + cfg.httpApiPort()
				+ "/api/queues";
		String json = readUrlToString(url, cfg.username(), cfg.password());
		QueueInfoList listInfo = GsonUtil.get().fromJson(json,
				QueueInfoList.class);
		if (listInfo != null) {
			for (QueueInfo info : listInfo) {
				System.out.println(info.getName());
			}
		}
		System.out.println();
	}

	protected static String readUrlToString(String url, String user, String pass)
			throws IOException {
		URL connectUrl = new URL(url);
		URLConnection yc = connectUrl.openConnection();
		String userpass = user + ":" + pass;
		String basicAuth = "Basic "
				+ new String(new Base64().encode(userpass.getBytes()));
		yc.setRequestProperty("Authorization", basicAuth);
		BufferedReader in = new BufferedReader(new InputStreamReader(
				yc.getInputStream()));
		StringBuffer result = new StringBuffer();
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			result.append(inputLine);
		}
		in.close();
		return result.toString();
	}
}
