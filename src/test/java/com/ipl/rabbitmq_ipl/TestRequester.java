package com.ipl.rabbitmq_ipl;

import com.ipl.annotation.RPCAnnotations.RPCHandlerClass;
import com.ipl.interfaces.RPCRequester;
import com.ipl.transfer.RPCFuture;
import com.ipl.transfer.RPCFutureIncremental;

@RPCHandlerClass(TestHandler.class)
public interface TestRequester extends RPCRequester {

	public void test1();

	public void test2(String name);

	public String test3(String name);

	public RPCFuture<String> test4();

	public RPCFutureIncremental<String> test5();
}
