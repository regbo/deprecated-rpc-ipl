package com.ipl.rabbitmq_ipl;

import com.ipl.interfaces.RPCHandler;
import com.ipl.transfer.RPCFuture;
import com.ipl.transfer.RPCFutureIncremental;

public class TestHandler implements RPCHandler, TestRequester {

	public void test1() {
		try {
			Thread.sleep(20 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("DONE VOID");
	}
	

	public void test2(String name) {
		// TODO Auto-generated method stub

	}

	public String test3(String name) {
		return new Object() {
		}.getClass().getEnclosingClass().getName() + " - test response: "
				+ name;
	}

	public RPCFuture<String> test4() {
		RPCFuture<String> resp = new RPCFuture<String>();
		resp.resultFuture.set(new Object() {
		}.getClass().getEnclosingClass().getName() + " - test rpc future resp");
		return resp;
	}

	public RPCFutureIncremental<String> test5() {
		final RPCFutureIncremental<String> resp = new RPCFutureIncremental<String>();
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					resp.fireIncrementalUpdateListener("upldate: " + i,
							(float) (i * (.2)));
					try {
						Thread.sleep(2 * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				resp.resultFuture.set("SLOW RESPONSE COMPLETE");

			}
		});
		thread.start();
		return resp;
	}

}
