package com.ipl.rabbitmq_ipl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.ipl.rpc.RPCFactory;
import com.ipl.transfer.RPCFutureIncremental;
import com.ipl.transfer.RPCFutureIncremental.IncrementalUpdateListener;

public class TestRPC {

	public static void main(String[] args) throws InterruptedException {
		// RPC.startHandling();
		TestRequester rpc = null;
		rpc.test1();
		for (int i = 0; i < 4; i++) {
			System.out.println(rpc.test3("cool"));
			Thread.sleep(1000);
		}
		System.out.println("starting 4");
		Futures.addCallback(rpc.test4().resultFuture, new FutureCallback<String>() {

			@Override
			public void onSuccess(String result) {
				System.out.println(result);
			}

			@Override
			public void onFailure(Throwable t) {
				t.printStackTrace();
			}
		}, MoreExecutors.directExecutor());
		System.out.println("done 4 init");
		RPCFutureIncremental<String> test5 = rpc.test5();
		Futures.addCallback(test5.resultFuture, new FutureCallback<String>() {

			@Override
			public void onSuccess(String result) {
				System.out.println(result);
			}

			@Override
			public void onFailure(Throwable t) {
				t.printStackTrace();
			}
		}, MoreExecutors.directExecutor());
		test5.addIncrementalUpdateListener(new IncrementalUpdateListener() {

			@Override
			public void incrementalUdpate(String msg, Float percentage) {
				System.out.println(msg + " - " + percentage);

			}
		});
	}
}
