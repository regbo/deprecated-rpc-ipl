package com.ipl.config;

import org.aeonbits.owner.Config;

public interface RabbitMQConfig extends Config {

	@DefaultValue("5672")
	int port();

	@DefaultValue("15672")
	int httpApiPort();

	String hostname();

	String username();

	String password();

	String virtualHost();

}