package com.ipl.config;

import org.aeonbits.owner.Config;

public interface RPCConfig extends Config {

	@DefaultValue("15")
	Integer numberOfConsumers();

}