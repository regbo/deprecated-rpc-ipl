package com.ipl.message;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.ipl.transfer.MessageSerializer;

public class RPCResponse {
	protected static final Logger logger = Logger.getLogger(RPCResponse.class);

	public static enum ResponseType {
		error, complete, incremental
	}

	private final ResponseType responseType;
	private String payload;
	private String throwableMsg;
	private String message;
	private Float percentage;
	private String correlationId;
	private String stacktrace;

	public RPCResponse(String correlationId, MessageSerializer serializer,
			Object object) {
		this.correlationId = correlationId;
		this.responseType = ResponseType.complete;
		this.payload = serializer.serializeResponsePayload(object);
	}

	public RPCResponse(String correlationId, String msg, Float percentage) {
		this.correlationId = correlationId;
		this.responseType = ResponseType.incremental;
		this.message = msg;
		this.percentage = percentage;
	}

	public RPCResponse(String correlationId, Throwable throwable) {
		this.correlationId = correlationId;
		this.responseType = ResponseType.error;
		this.throwableMsg = throwable != null
				&& !StringUtils.isBlank(throwable.getMessage()) ? throwable
				.getMessage() : "Unknown Error";
		this.stacktrace = ExceptionUtils.getStackTrace(throwable);

	}

	public String getMessage() {
		return message;
	}

	public Float getPercentage() {
		return percentage;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public String getThrowableMsg() {
		return throwableMsg;
	}

	public ResponseType getResponseType() {
		return responseType;
	}

	public String getPayload() {
		return payload;
	}

	public String getStacktraceAsString() {
		return stacktrace;
	}

}
