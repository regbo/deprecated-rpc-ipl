package com.ipl.message;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.ipl.interfaces.RPCRequester;
import com.ipl.transfer.MessageSerializer;

public class RPCRequest {

	private final String id = UUID.randomUUID().toString()
			.replaceAll("\\W+", "");
	private String methodName;
	private List<Class<?>> methodParameterTypes;
	private List<String> arguments;
	private Class<? extends RPCRequester> requesterClassType;
	private Class<? extends RPCRequester> handlerClassType;
	private boolean blockVoid = false;
	private Integer priority;
	private boolean resubmitOnException = false;

	protected <X extends RPCRequester> RPCRequest() {

	}

	public <X extends RPCRequester> RPCRequest(Class<X> requesterClassType,
			Method method) {
		this(requesterClassType, null, method);
	}

	public <X extends RPCRequester> RPCRequest(Class<X> requesterClassType,
			Class<? extends X> handlerClassType, Method method) {
		this(requesterClassType, handlerClassType, method, null, new Object[0]);
	}

	public <X extends RPCRequester> RPCRequest(Class<X> requesterClassType,
			Class<? extends X> handlerClassType, Method method,
			MessageSerializer serializer, Object... args) {
		this.requesterClassType = requesterClassType;
		this.methodName = method.getName();
		this.methodParameterTypes = method.getParameterTypes() != null ? Arrays
				.asList(method.getParameterTypes()) : null;
		this.handlerClassType = handlerClassType;
		if (args != null) {
			for (Object arg : args) {
				if (arguments == null) {
					arguments = new ArrayList<String>();
				}
				arguments.add(serializer.serializeRequestArgument(arg));
			}
		}
	}

	public String generateDefaultQueueName() {
		StringBuilder queueNameBuilder = new StringBuilder();
		queueNameBuilder.append(this.getRequesterClassType().getName());
		queueNameBuilder.append("_" + this.getMethodName());
		if (this.getMethodParameterTypes() != null)
			for (Class<?> rec : this.getMethodParameterTypes()) {
				queueNameBuilder.append("_" + rec.getName());
			}
		return queueNameBuilder.toString();
	}

	public String getId() {
		return id;
	}

	public boolean isBlockVoid() {
		return blockVoid;
	}

	public void setBlockVoid(boolean blockVoid) {
		this.blockVoid = blockVoid;
	}

	public List<String> getArguments() {
		return arguments;
	}

	public Class<?> getHandlerClassType() {
		return handlerClassType;
	}

	public Class<? extends RPCRequester> getRequesterClassType() {
		return requesterClassType;
	}

	public String getMethodName() {
		return methodName;
	}

	public List<Class<?>> getMethodParameterTypes() {
		return methodParameterTypes;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public boolean isResubmitOnException() {
		return resubmitOnException;
	}

	public void setResubmitOnException(boolean resubmitOnException) {
		this.resubmitOnException = resubmitOnException;
	}

}
