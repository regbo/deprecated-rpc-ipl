package com.ipl.transfer;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class RPCFutureIncremental<X> extends RPCFuture<X> {

	public CopyOnWriteArrayList<IncrementalUpdateListener> _listeners;

	public static class IncrementalUpdate {
		public final String msg;
		public final Float percentage;

		public IncrementalUpdate(String msg, Float perc) {
			this.msg = msg;
			this.percentage = perc;
		}

	}

	public List<IncrementalUpdateListener> getListeners() {
		if (_listeners == null) {
			synchronized (this) {
				if (_listeners == null) {
					_listeners = new CopyOnWriteArrayList<IncrementalUpdateListener>();
				}
			}
		}
		return _listeners;
	}

	public void fireIncrementalUpdateListener(final String msg,
			final Float percentage) {
		if (_listeners != null && _listeners.size() > 0) {
			for (IncrementalUpdateListener list : this.getListeners()) {
				list.incrementalUdpate(msg, percentage);
			}
		}
	}

	public void addIncrementalUpdateListener(IncrementalUpdateListener listener) {
		getListeners().add(listener);
	}

	public void removeIncrementalUpdateListener(
			IncrementalUpdateListener listener) {
		if (_listeners != null) {
			getListeners().remove(listener);
		}
	}

	public static interface IncrementalUpdateListener {

		public void incrementalUdpate(String msg, Float percentage);
	}
}
