package com.ipl.transfer;

import java.lang.reflect.Method;

import com.ipl.interfaces.RPCRequester;

public interface HandlerDiscoverer {

	public Class<?> getHandlerClass(Object self,
			Class<? extends RPCRequester> classType, Method method,
			Object[] args);

}
