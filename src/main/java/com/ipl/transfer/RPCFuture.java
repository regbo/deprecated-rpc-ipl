package com.ipl.transfer;

import com.google.common.util.concurrent.SettableFuture;

public class RPCFuture<X> {

	public final SettableFuture<X> resultFuture = SettableFuture.<X> create();

}
