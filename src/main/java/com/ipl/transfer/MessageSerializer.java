package com.ipl.transfer;

public interface MessageSerializer {

	public String serializeRequestArgument(Object obj);

	public Object deserializeRequestArgument(String argument);

	public String serializeResponsePayload(Object obj);

	public Object deserializeResponsePayload(String message);

}
