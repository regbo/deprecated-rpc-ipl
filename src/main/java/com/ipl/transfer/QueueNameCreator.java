package com.ipl.transfer;

import java.lang.reflect.Method;

import com.ipl.interfaces.RPCRequester;
import com.ipl.message.RPCRequest;

public interface QueueNameCreator {

	public <X extends RPCRequester> String generateQueueName(Object self,
			RPCRequest msg, Class<X> classType,
			Class<? extends X> handlerClass, Method method, Object[] args);
}
