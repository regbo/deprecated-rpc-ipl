package com.ipl.rpc;

public class ConstructorParameter {

	private Object value;
	private Class<?> classType;

	protected ConstructorParameter(Class<?> classType, Object value) {
		this.classType = classType;
		this.value = value;
	}

	public static ConstructorParameter fromObject(Object obj) {
		return new ConstructorParameter(obj.getClass(), obj);
	}

	public static ConstructorParameter fromClassType(Class<?> classType) {
		return new ConstructorParameter(classType, null);
	}

	public Object getValue() {
		return value;
	}

	public Class<?> getClassType() {
		return classType;
	}
}
