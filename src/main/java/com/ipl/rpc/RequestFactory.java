package com.ipl.rpc;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javassist.util.proxy.MethodFilter;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.Proxy;
import javassist.util.proxy.ProxyFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.log4j.Logger;
import org.reflections.ReflectionUtils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ipl.annotation.RPCAnnotations;
import com.ipl.annotation.RPCAnnotations.HandlerDiscovererClass;
import com.ipl.annotation.RPCAnnotations.RPCBlocking;
import com.ipl.annotation.RPCAnnotations.RPCHandlerClass;
import com.ipl.annotation.RPCAnnotations.RPCPriority;
import com.ipl.annotation.RPCAnnotations.ResubmitOnException;
import com.ipl.interfaces.RPCRequester;
import com.ipl.message.RPCRequest;
import com.ipl.message.RPCResponse;
import com.ipl.message.RPCResponse.ResponseType;
import com.ipl.rabbitmq.RabbitMQ;
import com.ipl.transfer.HandlerDiscoverer;
import com.ipl.transfer.MessageSerializer;
import com.ipl.transfer.QueueNameCreator;
import com.ipl.transfer.RPCFuture;
import com.ipl.transfer.RPCFutureIncremental;
import com.ipl.util.gson.GsonUtil;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.QueueingConsumer;

public class RequestFactory {
	private static Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());
	public static final String REQUEST_PREPEND = "RPC_REQ_";
	public static final String REPLY_PREPEND = "RPC_REPLY_";

	private final String instanceReplyQueue = REPLY_PREPEND + UUID.randomUUID();
	private boolean futureMonitorStarted = false;
	protected Cache<String, RPCFuture<?>> rpcFutureMap = CacheBuilder
			.newBuilder().expireAfterWrite(1, TimeUnit.HOURS).build();
	protected final Semaphore maxConcurrentCallbacks = new Semaphore(10);
	protected final ListeningExecutorService monitorExecutor = MoreExecutors
			.listeningDecorator(new ThreadPoolExecutor(0, Integer.MAX_VALUE,
					10l, TimeUnit.SECONDS, new SynchronousQueue<Runnable>()));
	private MessageSerializer serializer;

	public RequestFactory(MessageSerializer serializer) {
		this.serializer = serializer;
	}

	private void initFutureMonitor() {
		if (!futureMonitorStarted) {
			synchronized (RPCFactory.class) {
				if (!futureMonitorStarted) {
					monitorExecutor.submit(new Runnable() {

						@SuppressWarnings({ "unchecked", "rawtypes" })
						@Override
						public void run() {
							QueueingConsumer consumer = new QueueingConsumer(
									RabbitMQ.getChannel());
							try {
								declareReplyToQueue(instanceReplyQueue);
								RabbitMQ.getChannel().basicConsume(
										instanceReplyQueue, true, consumer);
								while (!Thread.currentThread().isInterrupted()) {
									QueueingConsumer.Delivery delivery = consumer
											.nextDelivery();
									String corrId = delivery.getProperties()
											.getCorrelationId();
									final RPCFuture future = rpcFutureMap
											.getIfPresent(corrId);
									if (future != null) {
										String json = new String(delivery
												.getBody());
										try {
											final RPCResponse resp = GsonUtil
													.get()
													.fromJson(
															json,
															com.ipl.message.RPCResponse.class);
											if (future instanceof RPCFutureIncremental
													&& ResponseType.incremental.equals(resp
															.getResponseType())) {
												final RPCFutureIncremental<?> incremental = (RPCFutureIncremental<?>) future;
												callback(new Runnable() {

													@Override
													public void run() {
														incremental
																.fireIncrementalUpdateListener(
																		resp.getMessage(),
																		resp.getPercentage());
													}
												});

											} else if (ResponseType.error
													.equals(resp
															.getResponseType())) {
												rpcFutureMap.invalidate(corrId);
												callback(new Runnable() {

													@Override
													public void run() {
														logger.info("failure during rpc");
														future.resultFuture
																.setException(new IOException(
																		resp.getThrowableMsg()
																				+ " - "
																				+ resp.getStacktraceAsString()));
													}
												});
											} else if (ResponseType.complete
													.equals(resp
															.getResponseType())) {
												rpcFutureMap.invalidate(corrId);
												callback(new Runnable() {

													@Override
													public void run() {
														Object payload = serializer
																.deserializeResponsePayload(resp
																		.getPayload());
														future.resultFuture
																.set(payload);
													}
												});

											} else {
												logger.error("could not handle callback: "
														+ json);

											}
										} catch (Exception e) {
											logger.error(
													"error during rpc callback execution",
													e);
										}
									}
								}
							} catch (Exception e) {
								logger.error(
										"general error in monitoring thread", e);
							}
						}
					});
				}
			}
		}
	}

	protected void callback(final Runnable runnable)
			throws InterruptedException {
		maxConcurrentCallbacks.acquire();
		monitorExecutor.submit(new Runnable() {

			@Override
			public void run() {
				try {
					runnable.run();
				} catch (Exception e) {
					logger.error("callback error", e);
				}
				maxConcurrentCallbacks.release();
			}
		});
	}

	private void sendMessage(String queueName, RPCRequest msg)
			throws IOException {
		String requestQueue = REQUEST_PREPEND
				+ (StringUtils.isBlank(queueName) ? msg
						.generateDefaultQueueName() : queueName);
		RabbitMQ.queueDeclare(requestQueue, true, false, false, null);
		String corrId = msg.getId();
		BasicProperties props = new BasicProperties.Builder()
				.correlationId(corrId).replyTo(instanceReplyQueue).build();
		String json = GsonUtil.get().toJson(msg);
		RabbitMQ.getChannel().basicPublish("", requestQueue, props,
				json.getBytes());
	}

	private void initiateFuture(String queueName, final RPCFuture<?> future,
			RPCRequest msg) throws IOException {
		initFutureMonitor();
		rpcFutureMap.put(msg.getId(), future);
		sendMessage(queueName, msg);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <X extends RPCRequester> X create(final Class<X> classType,
			final Class<? extends X> handlerClassType,
			final MessageSerializer serializer,
			List<ConstructorParameter> argList) throws NoSuchMethodException,
			IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		ProxyFactory factory = new ProxyFactory();
		if (classType.isInterface()) {
			factory.setInterfaces(new Class[] { classType });
		} else {
			factory.setSuperclass(classType);
		}
		factory.setFilter(new MethodFilter() {
			@Override
			public boolean isHandled(Method method) {
				boolean result = !("finalize".equalsIgnoreCase(method.getName()) && method
						.getParameterTypes().length == 0)
						&& Modifier.isAbstract(method.getModifiers());
				return result;
			}
		});
		MethodHandler methodHandler = new MethodHandler() {

			@Override
			public Object invoke(Object self, Method method, Method proceed,
					Object[] args) throws Throwable {
				try {
					Class handlerClass = handlerClassType;
					if (handlerClass == null) {
						RPCHandlerClass anno = classType
								.getAnnotation(RPCAnnotations.RPCHandlerClass.class);
						handlerClass = anno != null ? anno.value() : null;
					}
					if (handlerClass == null) {
						HandlerDiscovererClass anno = classType
								.getAnnotation(RPCAnnotations.HandlerDiscovererClass.class);
						Class<? extends HandlerDiscoverer> handleClassDisc = anno != null ? anno
								.value() : null;
						if (handleClassDisc != null)
							try {
								HandlerDiscoverer disc = handleClassDisc
										.newInstance();
								handlerClass = disc.getHandlerClass(self,
										classType, method, args);
							} catch (Exception e) {
								logger.error("could not resolve handler", e);
							}
					}
					RPCRequest msg = new RPCRequest(classType, handlerClass,
							method, serializer, args);
					Integer msgPriority = null;
					for (Object obj : Arrays.asList(classType, method)) {
						if (obj != null) {
							RPCPriority rpcPriority = null;
							if (obj instanceof Method) {
								rpcPriority = ((Method) obj)
										.getAnnotation(RPCPriority.class);
							} else if (obj instanceof Class) {
								rpcPriority = ((Class<?>) obj)
										.getAnnotation(RPCPriority.class);
							}
							if (rpcPriority != null && rpcPriority.value() > 0) {
								msgPriority = rpcPriority.value();
							}
						}
					}
					if (msgPriority != null) {
						msg.setPriority(msgPriority);
					}
					final MutableObject<String> customQueueName = new MutableObject<String>();
					{
						com.ipl.annotation.RPCAnnotations.QueueNameCreatorClass creatorAnno = classType
								.getAnnotation(RPCAnnotations.QueueNameCreatorClass.class);
						if (creatorAnno != null && creatorAnno.value() != null) {
							try {
								QueueNameCreator creator = creatorAnno.value()
										.newInstance();
								customQueueName.setValue(creator
										.generateQueueName(self, msg,
												classType, handlerClass,
												method, args));
							} catch (Exception e) {
								logger.error("could not create queue name", e);
							}
						}
					}
					{
						ResubmitOnException resub = method
								.getAnnotation(RPCAnnotations.ResubmitOnException.class);
						msg.setResubmitOnException(resub != null ? resub
								.value() : false);
					}
					RPCFuture future = null;
					if (method.getReturnType().equals(Void.TYPE)) {
						RPCBlocking blocking = method
								.getAnnotation(RPCAnnotations.RPCBlocking.class);
						if (blocking != null && blocking.value()) {
							msg.setBlockVoid(true);
							future = new RPCFuture();
						}
					} else if (RPCFutureIncremental.class
							.isAssignableFrom(method.getReturnType())) {
						future = new RPCFutureIncremental();
					} else {
						future = new RPCFuture();
					}
					if (future != null) {
						initiateFuture(customQueueName.getValue(), future, msg);
					} else {
						sendMessage(customQueueName.getValue(), msg);
					}
					if (future != null
							&& RPCFuture.class.isAssignableFrom(method
									.getReturnType())) {
						return future;
					} else if (future != null) {
						return future.resultFuture.get();
					}
				} catch (Exception e) {
					logger.error("uncaught request exception", e);
				}
				return null;
			}

		};
		List<Class<?>> argClassTypes = new ArrayList<Class<?>>();
		List<Object> args = new ArrayList<Object>();
		if (argList != null)
			for (ConstructorParameter entry : argList) {
				argClassTypes.add(entry.getClassType());
				args.add(entry.getValue());
			}
		Class[] typeArr = argClassTypes
				.toArray(new Class[argClassTypes.size()]);
		Object[] argArr = args.toArray(new Object[args.size()]);
		Class<?> newClass = factory.createClass();
		Object obj = null;
		try {
			obj = newClass.getConstructor(typeArr).newInstance(argArr);

		} catch (NoSuchMethodException e) {
			try {
				obj = newClass.getDeclaredConstructor(typeArr).newInstance(
						argArr);
			} catch (NoSuchMethodException e1) {
				for (Constructor<?> checkCon : ReflectionUtils
						.getAllConstructors(newClass, ReflectionUtils
								.withParametersAssignableTo(typeArr))) {
					checkCon.setAccessible(true);
					try {
						obj = checkCon.newInstance(argArr);
						break;
					} catch (Exception e2) {
					}
				}
			}
		}
		((Proxy) obj).setHandler(methodHandler);
		return (X) obj;
	}

	public void shutdown() {
		monitorExecutor.shutdownNow();
	}

	public static void declareReplyToQueue(String replyTo) throws IOException {
		RabbitMQ.queueDeclare(replyTo, false, false, true, null);
	}

}
