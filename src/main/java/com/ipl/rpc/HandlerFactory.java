package com.ipl.rpc;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.aeonbits.owner.ConfigFactory;
import org.apache.log4j.Logger;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ipl.config.RPCConfig;
import com.ipl.queueInfo.QueueInfo;
import com.ipl.rabbitmq.RabbitMQ;
import com.ipl.rpc.consumer.QueueDeliveryProcessor;
import com.ipl.transfer.MessageSerializer;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

public class HandlerFactory {
	private static Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());
	private static final TimeUnit maxRPCRunTimeUnit = TimeUnit.MINUTES;
	private static final int maxRpcRunDuration = 30;

	// eo static

	protected Cache<String, Boolean> banCache = CacheBuilder.newBuilder()
			.expireAfterWrite(5, TimeUnit.MINUTES).build();
	protected final ListeningScheduledExecutorService updateExecutor = MoreExecutors
			.listeningDecorator(Executors.newScheduledThreadPool(1));
	protected final ListeningExecutorService queueExecutor = MoreExecutors
			.listeningDecorator(Executors.newCachedThreadPool());
	private final Map<String, ListenableFuture<?>> producerMap = Collections
			.synchronizedMap(new HashMap<String, ListenableFuture<?>>());

	private MessageSerializer serializer;
	private Runnable _rpcHandleRunnable;
	private LoadingCache<Integer, Semaphore> prioritySemaphoreCache;

	public HandlerFactory(MessageSerializer serializer) {
		this(serializer, null);
	}

	public HandlerFactory(MessageSerializer serializer,
			Integer setNumberOfConsumers) {
		this.serializer = serializer;
		final int semaphorePerPriority;
		if (setNumberOfConsumers != null) {
			semaphorePerPriority = setNumberOfConsumers;
		} else {
			semaphorePerPriority = ConfigFactory.create(RPCConfig.class)
					.numberOfConsumers();
		}
		this.prioritySemaphoreCache = CacheBuilder.newBuilder().build(
				new CacheLoader<Integer, Semaphore>() {

					@Override
					public Semaphore load(Integer key) throws Exception {
						return new Semaphore(semaphorePerPriority, true);
					}
				});

	}

	public void startHandling() {
		if (_rpcHandleRunnable == null) {
			init_rpcHandleRunnable();
		}
	}

	private synchronized void init_rpcHandleRunnable() {
		if (_rpcHandleRunnable == null) {
			Runnable queueMon = new Runnable() {
				private volatile boolean running = false;

				@Override
				public void run() {
					if (!running) {
						running = true;
						Thread thread = new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									Set<String> allQueues = getUnbannedQueues();
									killFuturesWithNoQueue(allQueues);
									Set<String> newQueues = getNewQueues(allQueues);
									for (final String queue : newQueues) {
										QueueingConsumer consumer = null;
										try {
											Channel channel = RabbitMQ
													.getChannel();
											consumer = new QueueingConsumer(
													channel);
											channel.basicConsume(queue, false,
													consumer);
											if (channel != null
													&& consumer != null) {
												ListenableFuture<?> furure = queueExecutor
														.submit(new QueueDeliveryProcessor(
																queue,
																consumer,
																prioritySemaphoreCache,
																queueExecutor,
																banCache,
																serializer,
																maxRPCRunTimeUnit,
																maxRpcRunDuration));
												producerMap.put(queue, furure);
												logger.info("starting producer: "
														+ queue);
											}
										} catch (Exception e) {
											logger.error(
													"could not add consumer to: "
															+ queue, e);
											banCache.put(queue, true);
											throw e;
										}
									}
								} catch (Exception e) {
									logger.error(
											"queue get and consume failed", e);
								}
								running = false;
							}

						});
						thread.start();
					}
				}
			};
			updateExecutor.scheduleAtFixedRate(queueMon, 0, 10,
					TimeUnit.SECONDS);
			_rpcHandleRunnable = queueMon;
		}
	}

	private void killFuturesWithNoQueue(Set<String> queueNames) {
		Set<String> removeKeys = new HashSet<String>();
		for (Entry<String, ListenableFuture<?>> entry : producerMap.entrySet()) {
			if (queueNames == null || !queueNames.contains(entry.getKey())
					|| entry.getValue().isDone()) {
				entry.getValue().cancel(true);
				removeKeys.add(entry.getKey());
			}
		}
		for (String rem : removeKeys) {
			producerMap.remove(rem);
		}
	}

	@SuppressWarnings("unchecked")
	private Set<String> getNewQueues(Set<String> queueNames) {
		if (queueNames == null || queueNames.size() == 0) {
			return Collections.EMPTY_SET;
		}
		Set<String> newQueues = new HashSet<String>(queueNames);
		newQueues.removeAll(this.producerMap.keySet());
		return newQueues;
	}

	private Set<String> getUnbannedQueues() {
		Set<String> queueNames = new HashSet<String>();
		for (QueueInfo info : RabbitMQ.getQueueInfoList()) {
			if (info.getName().startsWith(RequestFactory.REQUEST_PREPEND)) {
				Boolean ban = banCache.getIfPresent(info.getName());
				if (ban == null || !ban) {
					queueNames.add(info.getName());
				}
			}
		}
		return queueNames;
	}

	public void shutdown() {
		for (ListeningExecutorService exec : Arrays.asList(this.queueExecutor,
				this.updateExecutor)) {
			try {
				exec.shutdownNow();
			} catch (Exception e) {
				logger.error("error during proc shutdown", e);
			}
		}
	}

}
