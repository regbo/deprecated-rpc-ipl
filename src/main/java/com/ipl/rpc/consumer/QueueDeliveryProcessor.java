package com.ipl.rpc.consumer;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.cache.Cache;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.ipl.rabbitmq.RabbitMQ;
import com.ipl.transfer.MessageSerializer;
import com.ipl.util.Rand;
import com.ipl.util.gson.GsonUtil;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;

public class QueueDeliveryProcessor extends InfiniteRunnable {

	private static final Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());
	private static Thread statsTracker;
	private static final Map<String, Date> outstandingRPC = Collections
			.synchronizedMap(new LinkedHashMap<String, Date>());
	private QueueingConsumer consumer;
	private String queueName;
	private MessageSerializer serializer;
	private Cache<String, Boolean> banCache;
	private TimeUnit maxRPCRunTimeUnit;
	private int maxRpcRunDuration;
	private ListeningExecutorService executorService;
	private LoadingCache<Integer, Semaphore> prioritySemaphoreCache;

	public QueueDeliveryProcessor(String queueName, QueueingConsumer consumer,
			LoadingCache<Integer, Semaphore> prioritySemaphoreCache,
			ListeningExecutorService executorService,
			Cache<String, Boolean> banCache, MessageSerializer serializer,
			TimeUnit maxRPCRunTimeUnit, int maxRpcRunDuration) {
		this.queueName = queueName;
		this.consumer = consumer;
		this.prioritySemaphoreCache = prioritySemaphoreCache;
		this.executorService = executorService;
		this.banCache = banCache;
		this.serializer = serializer;
		this.maxRPCRunTimeUnit = maxRPCRunTimeUnit;
		this.maxRpcRunDuration = maxRpcRunDuration;
		if (StringUtils.containsIgnoreCase(System.getProperty("os.name"),
				"window")) {
			if (statsTracker == null) {
				synchronized (outstandingRPC) {
					if (statsTracker == null) {
						logger.info("verbose logging enabled");
						Thread tracker = new Thread(new Runnable() {
							private Date logAt;

							@Override
							public void run() {
								while (true) {
									if (logAt == null) {
										updateLogAt();
									}
									try {
										Thread.sleep(Rand
												.randBetween(500, 1000));
									} catch (InterruptedException e) {
										logger.warn("interupt", e);
									}
									if (new Date().after(logAt)) {
										synchronized (outstandingRPC) {
											System.err
													.println("****RPC LOG****\n"
															+ GsonUtil
																	.get()
																	.toJson(outstandingRPC)
															+ "\n****RPC LOG****");
											updateLogAt();
										}
									}
								}
							}

							private void updateLogAt() {
								logAt = new Date(new Date().getTime()
										+ (10 * 1000));
							}
						});
						tracker.start();
						statsTracker = tracker;
					}
				}
			}
		}
	}

	@Override
	public void infiniteProcess() throws Exception {
		Delivery deliv = consumer.nextDelivery();
		final RPCRequestWrap requestWrap;
		try {
			requestWrap = deliv != null ? new RPCRequestWrap(deliv,
					this.queueName) : null;
			if (requestWrap == null) {
				throw new IOException();
			}
		} catch (IOException e) {
			if (deliv != null) {
				RabbitMQ.getChannel().basicNack(
						deliv.getEnvelope().getDeliveryTag(), false, true);
			}
			logger.error("error during rpc request wrap createion", e);
			throw e;
		}
		logger.info("delivery from queue submitting to executor: " + queueName);
		final String logKey;
		if (statsTracker != null) {
			logKey = GsonUtil.get().toJson(requestWrap.getRequest());
			synchronized (outstandingRPC) {
				outstandingRPC.put(logKey, new Date());
			}
		} else {
			logKey = null;
		}
		final Semaphore semaPhore = prioritySemaphoreCache.get(requestWrap
				.extractPriority());
		final RPCRequestCallable callable = new RPCRequestCallable(requestWrap,
				banCache, serializer, maxRPCRunTimeUnit, maxRpcRunDuration);
		semaPhore.acquire();
		ListenableFuture<Object> future = executorService.submit(callable);
		Futures.addCallback(future, new FutureCallback<Object>() {

			@Override
			public void onSuccess(Object ignore) {
				semaPhore.release();
				requestWrap.autoAckOrNack(false);
				debugLogDone();
			}

			@Override
			public void onFailure(Throwable t) {
				semaPhore.release();
				requestWrap.autoAckOrNack(true);
				logger.error(
						"rpc error: "
								+ GsonUtil.get().toJson(
										requestWrap.getRequest()), t);
				debugLogDone();
			}

			private void debugLogDone() {
				if (logKey != null) {
					synchronized (outstandingRPC) {
						outstandingRPC.remove(logKey);
					}
				}
			}

		}, MoreExecutors.directExecutor());

	}

	@Override
	public void onError(Throwable t) {
		logger.error("uncaught rpc error: " + queueName, t);
	}

}
