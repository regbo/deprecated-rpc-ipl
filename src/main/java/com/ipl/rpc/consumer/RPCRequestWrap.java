package com.ipl.rpc.consumer;

import java.io.IOException;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;

import com.google.gson.JsonSyntaxException;
import com.ipl.message.RPCRequest;
import com.ipl.rabbitmq.RabbitMQ;
import com.ipl.util.gson.GsonUtil;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.QueueingConsumer.Delivery;

public class RPCRequestWrap {

	private static final Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());
	private Delivery delivery;
	private BasicProperties msgProps;
	private String queueName;
	private boolean alreadyAckNack;
	private RPCRequest request;

	public RPCRequestWrap(Delivery delivery, String queueName)
			throws IOException {
		Validate.isTrue(delivery != null, "deliv is null");
		try {
			this.request = parseRequest(delivery);
		} catch (Exception e) {
			throw new IOException(e);
		}
		this.delivery = delivery;
		this.queueName = queueName;
		this.msgProps = delivery.getProperties();
	}

	public int extractPriority() {
		RPCRequest payload = this.getRequest();
		if (payload != null && payload.getPriority() != null
				&& payload.getPriority() > 0) {
			return payload.getPriority();
		}
		return 0;
	}

	private static RPCRequest parseRequest(Delivery deliv)
			throws JsonSyntaxException {
		String currentMsgJson = new String(deliv.getBody());
		return GsonUtil.get().fromJson(currentMsgJson,
				com.ipl.message.RPCRequest.class);

	}

	public Delivery getDelivery() {
		return delivery;
	}

	public BasicProperties getMsgProps() {
		return msgProps;
	}

	public String getQueueName() {
		return queueName;
	}

	public void autoAckOrNack(boolean error) {
		if (!alreadyAckNack) {
			synchronized (this) {
				if (!alreadyAckNack) {
					try {
						if (error && this.getRequest().isResubmitOnException()) {
							RabbitMQ.getChannel().basicNack(
									this.getDelivery().getEnvelope()
											.getDeliveryTag(), false, true);

						} else {
							RabbitMQ.getChannel().basicAck(
									this.getDelivery().getEnvelope()
											.getDeliveryTag(), false);
						}
						alreadyAckNack = true;
					} catch (IOException e) {
						logger.error("auto ack failed: "
								+ GsonUtil.get().toJson(this.getRequest()));
					}
				}
			}
		}
	}

	public RPCRequest getRequest() {
		return request;
	}

}
