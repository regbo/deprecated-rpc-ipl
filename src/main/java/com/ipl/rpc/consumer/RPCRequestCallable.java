package com.ipl.rpc.consumer;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.log4j.Logger;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

import com.google.common.base.Predicate;
import com.google.common.cache.Cache;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.ipl.message.RPCRequest;
import com.ipl.message.RPCResponse;
import com.ipl.rabbitmq.RabbitMQ;
import com.ipl.rpc.RequestFactory;
import com.ipl.transfer.MessageSerializer;
import com.ipl.transfer.RPCFuture;
import com.ipl.transfer.RPCFutureIncremental;
import com.ipl.transfer.RPCFutureIncremental.IncrementalUpdateListener;
import com.ipl.util.gson.GsonUtil;
import com.rabbitmq.client.AMQP.BasicProperties;

public class RPCRequestCallable implements Callable<Object>,
		Comparable<RPCRequestCallable> {

	private static final Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());
	private static final long LONG_RUN_WARN_MINUTES = 10;
	private RPCRequestWrap requestWrap;
	private Cache<String, Boolean> banCache;
	private MessageSerializer serializer;
	private int maxRpcRunDuration;
	private TimeUnit maxRPCRunTimeUnit;

	public RPCRequestCallable(RPCRequestWrap requestWrap,
			Cache<String, Boolean> banCache, MessageSerializer serializer,
			TimeUnit maxRPCRunTimeUnit, int maxRpcRunDuration) {
		this.requestWrap = requestWrap;
		this.banCache = banCache;
		this.serializer = serializer;
		this.maxRPCRunTimeUnit = maxRPCRunTimeUnit;
		this.maxRpcRunDuration = maxRpcRunDuration;
	}

	@Override
	public Object call() throws Exception {
		final MutableObject<RPCResponse> finalResponse = new MutableObject<RPCResponse>();
		Class<?> handlerClass = resolveHandlerClass(requestWrap.getRequest());
		if (handlerClass == null) {
			banCache.put(requestWrap.getQueueName(), true);
		} else {
			Object val = handlerClass.newInstance();
			Object[] argArr = extractArgs(serializer, requestWrap.getRequest());
			Method currentMethod = resolveMethod(requestWrap.getRequest(),
					handlerClass);
			Object result = null;
			Exception invokeExp = null;
			try {
				result = currentMethod.invoke(val, argArr);
			} catch (Exception e) {
				invokeExp = e;
			}
			ReturnType retType = parseReturnType(currentMethod, requestWrap
					.getRequest().isBlockVoid());
			if (invokeExp != null) {
				if (ReturnType.noReturn != retType) {
					invokeExp = invokeExp != null ? invokeExp
							: new IOException(
									"result marked as error with null exception?");
					finalResponse.setValue(new RPCResponse(requestWrap
							.getRequest().getId(), serializer, invokeExp));
				}
				throw invokeExp;
			} else if (ReturnType.noReturn != retType) {
				RPCFuture<?> future = result != null
						&& result instanceof RPCFuture ? (RPCFuture<?>) result
						: null;
				if (future == null) {
					finalResponse.setValue(new RPCResponse(requestWrap
							.getRequest().getId(), serializer, result));
				} else {
					if (future instanceof RPCFutureIncremental) {
						RPCFutureIncremental<?> inc = (RPCFutureIncremental<?>) future;
						inc.addIncrementalUpdateListener(new IncrementalUpdateListener() {

							@Override
							public void incrementalUdpate(String msg,
									Float percentage) {
								try {
									publishResponse(requestWrap.getMsgProps(),
											new RPCResponse(requestWrap
													.getRequest().getId(), msg,
													percentage));
								} catch (IOException e) {
									logger.error(
											"failed to fire incremental update",
											e);
								}

							}
						});
					}
					final CountDownLatch callbackLatch = new CountDownLatch(1);
					Futures.addCallback(future.resultFuture,
							new FutureCallback<Object>() {

								@Override
								public void onSuccess(Object result) {
									RPCResponse resp = new RPCResponse(
											requestWrap.getRequest().getId(),
											serializer, result);
									done(resp);

								}

								private void done(RPCResponse resp) {
									finalResponse.setValue(resp);
									callbackLatch.countDown();
								}

								@Override
								public void onFailure(Throwable t) {
									logger.error("rpc callback failure", t);
									RPCResponse resp = new RPCResponse(
											requestWrap.getRequest().getId(), t);
									done(resp);
								}
							}, MoreExecutors.directExecutor());
					Date killAt = new Date(new Date().getTime()
							+ (maxRPCRunTimeUnit.toMillis(maxRpcRunDuration)));
					try {
						while (!callbackLatch.await(LONG_RUN_WARN_MINUTES,
								TimeUnit.MINUTES) && new Date().before(killAt)) {
							logger.warn("rpc callback has been stuck for "
									+ LONG_RUN_WARN_MINUTES
									+ " minutes: "
									+ GsonUtil.get().toJson(
											requestWrap.getRequest()));
						}
						if (callbackLatch.getCount() > 0) {
							future.resultFuture.cancel(true);
							logger.error("rpc queue call failed to complete: "
									+ GsonUtil.get().toJson(
											requestWrap.getRequest()));
						}
					} catch (Exception e) {
						logger.error(
								"waiting for rpc interupted or kill rpc failed",
								e);
					}
				}
			}
		}
		if (finalResponse.getValue() != null) {
			publishResponse(requestWrap.getMsgProps(), finalResponse.getValue());
		}
		return null;
	}

	@Override
	public int compareTo(RPCRequestCallable o) {
		return new CompareToBuilder().append(
				this.requestWrap.extractPriority(),
				o.getRequestWrap().extractPriority()).toComparison()
				* -1;
	}

	public RPCRequestWrap getRequestWrap() {
		return requestWrap;
	}

	protected static Class<?> resolveHandlerClass(RPCRequest currentRPCMessage) {
		Class<?> handlerClass = currentRPCMessage.getHandlerClassType();
		if (handlerClass == null) {
			logger.warn("no handler class specified, trying to find one. this can be dangerous");
			Reflections reflections = new Reflections();
			for (Class<?> classType : reflections
					.getSubTypesOf(currentRPCMessage.getRequesterClassType())) {
				if (!classType.isInterface()
						&& !Modifier.isAbstract(classType.getModifiers())) {
					handlerClass = classType;
					break;
				}
			}
		}
		return handlerClass;
	}

	protected static Object[] extractArgs(MessageSerializer serializer,
			RPCRequest currentRPCMessage) {
		ArrayList<Object> args = new ArrayList<Object>();
		List<String> payloads = currentRPCMessage.getArguments();
		if (payloads != null) {
			for (String load : payloads) {
				args.add(serializer.deserializeRequestArgument(load));
			}
		}
		Object[] argArr = args == null || args.size() <= 0 ? new Object[0]
				: args.toArray(new Object[args.size()]);
		return argArr;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static Method resolveMethod(RPCRequest value,
			Class<?> handlerClass) {
		List<Class<?>> paramTypes = new ArrayList<Class<?>>();
		for (Class<?> arg : value.getMethodParameterTypes()) {
			paramTypes.add(arg == null ? Object.class : arg);
		}
		List<Predicate<?>> predicates = new ArrayList<Predicate<?>>();
		predicates.add(ReflectionUtils.withName(value.getMethodName()));
		predicates.add(ReflectionUtils.withParametersCount(paramTypes.size()));
		if (paramTypes.size() > 0) {
			predicates.add(ReflectionUtils
					.withParametersAssignableTo(paramTypes
							.toArray(new Class[paramTypes.size()])));
		}
		Set methodObjs = ReflectionUtils.getAllMethods(
				value.getRequesterClassType(),
				predicates.toArray(new Predicate[predicates.size()]));
		for (Object methObj : methodObjs) {
			if (methObj instanceof Method) {
				((Method) methObj).setAccessible(true);
				return (Method) methObj;
			}
		}
		return null;
	}

	protected static void publishResponse(BasicProperties props,
			RPCResponse resp) throws IOException {
		logger.info("publishing response");
		RequestFactory.declareReplyToQueue(props.getReplyTo());
		RabbitMQ.getChannel().basicPublish("", props.getReplyTo(), props,
				GsonUtil.get().toJson(resp).getBytes());
		logger.info("done publishing response");
	}

	public static ReturnType parseReturnType(Method currentMethod,
			boolean blockVoid) {
		boolean isVoid = currentMethod != null
				&& currentMethod.getReturnType() != null
				&& currentMethod.getReturnType().equals(Void.TYPE);
		boolean isRpcFuture = currentMethod != null
				&& currentMethod.getReturnType() != null
				&& currentMethod.getReturnType().isAssignableFrom(
						RPCFuture.class);
		if (isVoid && !blockVoid) {
			return ReturnType.noReturn;

		} else if (isVoid) {
			return ReturnType.blockingVoidRet;
		} else if (isRpcFuture) {
			return ReturnType.rpcFuture;
		} else {
			return ReturnType.objectRet;
		}

	}

	protected static enum ReturnType {
		noReturn, blockingVoidRet, rpcFuture, objectRet

	}
}
