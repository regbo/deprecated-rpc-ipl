package com.ipl.rpc.consumer;

import org.apache.log4j.Logger;

public abstract class InfiniteRunnable implements Runnable {

	private static final Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());
	private volatile boolean running = true;
	private volatile boolean shutdown;

	@Override
	public void run() {
		while (running && !Thread.interrupted()) {
			try {
				infiniteProcess();
			} catch (Throwable e) {
				try {
					onError(e);
				} catch (Throwable t) {
					logger.error("could not handle onerror", e);
				}
			}
		}
		shutdown = true;
	}

	public abstract void infiniteProcess() throws Exception;

	public abstract void onError(Throwable t);

	public void requestShutdown() {
		this.running = false;
	}

	public boolean isShutdown() {
		return shutdown;
	}
}
