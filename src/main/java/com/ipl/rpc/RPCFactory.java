package com.ipl.rpc;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ipl.interfaces.RPCRequester;
import com.ipl.transfer.MessageSerializer;

public class RPCFactory {
	private static Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());

	public static RPCFactory createFactory(
			Class<? extends MessageSerializer> serializerClassType)
			throws InstantiationException {
		try {
			return new RPCFactory(serializerClassType.newInstance());
		} catch (IllegalAccessException e) {
			throw new InstantiationException(e.getMessage());
		}
	}

	private MessageSerializer serializer;
	private HandlerFactory _handlerFactory;
	private RequestFactory _requestFactory;

	protected RPCFactory(MessageSerializer serializer) {
		this.serializer = serializer;
	}

	public void shutdown() {
		if (_handlerFactory != null) {
			_handlerFactory.shutdown();
		}
		if (_requestFactory != null) {
			_requestFactory.shutdown();
		}
	}

	public <X extends RPCRequester> X getRequester(Class<X> classType) {
		return (X) getRequester(classType, null);
	}

	public <X extends RPCRequester> X getRequester(Class<X> classType,
			Class<? extends X> handlerClassType) {
		return (X) getRequester(classType, handlerClassType,
				new ConstructorParameter[0]);
	}

	public <X extends RPCRequester> X getRequester(Class<X> classType,
			Class<? extends X> handlerClassType, ConstructorParameter... args) {
		try {
			List<ConstructorParameter> argList = new ArrayList<ConstructorParameter>();
			if (args != null) {
				for (ConstructorParameter entry : args) {
					argList.add(entry);
				}
			}
			return getRequestFactory().create(classType, handlerClassType,
					serializer, argList);
		} catch (NoSuchMethodException | IllegalArgumentException
				| InstantiationException | IllegalAccessException
				| InvocationTargetException e) {
			logger.error("could not create proxy", e);
		}
		return null;
	}

	private RequestFactory getRequestFactory() {
		if (_requestFactory == null) {
			init_requestFactory();
		}
		return _requestFactory;

	}

	private synchronized void init_requestFactory() {
		if (_requestFactory == null) {
			_requestFactory = new RequestFactory(serializer);
		}

	}

	public HandlerFactory getHandlerFactory() {
		if (_handlerFactory == null) {
			init_handlerFactory();
		}
		return _handlerFactory;
	}

	private synchronized void init_handlerFactory() {
		if (_handlerFactory == null) {
			_handlerFactory = new HandlerFactory(serializer);
		}
	}

	public void startHandling() {
		HandlerFactory handler = getHandlerFactory();
		handler.startHandling();
	}
}
