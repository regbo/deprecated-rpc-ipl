package com.ipl.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ipl.interfaces.RPCHandler;
import com.ipl.transfer.HandlerDiscoverer;

public class RPCAnnotations {

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.TYPE })
	public @interface RPCHandlerClass {
		Class<? extends RPCHandler> value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.TYPE })
	public @interface QueueNameCreatorClass {
		Class<? extends com.ipl.transfer.QueueNameCreator> value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.TYPE })
	public @interface HandlerDiscovererClass {
		Class<? extends HandlerDiscoverer> value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.METHOD })
	public @interface RPCBlocking {
		boolean value() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.METHOD })
	public @interface ResubmitOnException {
		boolean value() default true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.METHOD, ElementType.FIELD })
	public @interface RPCPriority {
		int value() default 0;
	}

}
