package com.ipl.util.cache;

public class ClassTypeCacheKey<T> {
	public final String classTypeStr;
	public final Class<T> classType;

	private ClassTypeCacheKey(Class<T> type) {
		this.classType = type;
		this.classTypeStr = type.getName();
	}

	public static <X> ClassTypeCacheKey<X> create(Class<X> classType) {
		return new ClassTypeCacheKey<X>(classType);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classTypeStr == null) ? 0 : classTypeStr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassTypeCacheKey other = (ClassTypeCacheKey) obj;
		if (classTypeStr == null) {
			if (other.classTypeStr != null)
				return false;
		} else if (!classTypeStr.equals(other.classTypeStr))
			return false;
		return true;
	}
}
