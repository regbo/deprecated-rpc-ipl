package com.ipl.util.gson;

import java.lang.reflect.Type;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.reflections.ReflectionUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class GsonUtil {
	private static Logger logger = Logger.getLogger(new Object() {
	}.getClass().getEnclosingClass());

	private static Gson _gson;

	public static Gson get() {
		if (_gson == null) {
			init_gson();
		}
		return _gson;
	}

	private synchronized static void init_gson() {
		if (_gson == null) {
			GsonBuilder gsonBilder = new GsonBuilder();
			JsonDeserializerAndSerializer<Class<?>> classSer = new JsonDeserializerAndSerializer<Class<?>>() {
				@Override
				public JsonElement serialize(Class<?> classType,
						Type typeOfSrc, JsonSerializationContext context) {
					if (classType == null) {
						return null;
					} else {
						JsonPrimitive jo = new JsonPrimitive(
								classType.getName());
						return jo;
					}
				}

				@Override
				public Class<?> deserialize(JsonElement json, Type typeOfT,
						JsonDeserializationContext context)
						throws JsonParseException {
					if (json == null || !(json instanceof JsonPrimitive)) {
						return null;
					}
					String val = json.getAsString();
					if (!StringUtils.isBlank(val)) {
						try {
							return ReflectionUtils.forName(val);
						} catch (Exception e) {
							logger.error("could not parse type: " + val, e);
						}

					}
					return null;
				}

				@SuppressWarnings({ "unchecked", "rawtypes" })
				@Override
				public Class getClassType() {
					return Class.class;
				}
			};
			_gson = gsonBilder.registerTypeAdapter(classSer.getClassType(),
					classSer).create();
		}
	}

	public static abstract class JsonDeserializerAndSerializer<X> implements
			JsonDeserializer<X>, JsonSerializer<Class<?>> {

		public abstract Class<X> getClassType();
	}
}
