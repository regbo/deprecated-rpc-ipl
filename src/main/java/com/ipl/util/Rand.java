package com.ipl.util;

import java.util.Random;

public class Rand {
	private static final Random rand = new Random();

	public static int randBetween(int bound1, int bound2) {
		int min = Math.min(bound1, bound2);
		int max = Math.max(bound1, bound2);
		return rand.nextInt(max - min) + min;
	}
}
