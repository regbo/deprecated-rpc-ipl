package com.ipl.rabbitmq;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import java.util.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.ipl.config.RabbitMQConfig;
import com.ipl.queueInfo.QueueInfoList;
import com.ipl.util.gson.GsonUtil;
import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMQ {
	protected static Logger logger = Logger.getLogger(RabbitMQ.class);
	private static Optional<Channel> _rabbitMQChannel;
	private static int maxConnectionTry = 3;
	private static boolean connectionDead = false;

	private static Optional<Connection> _connectionOp;
	private static ConcurrentHashMap<String, DeclareOk> declareMap = new ConcurrentHashMap<String, DeclareOk>();

	private static LoadingCache<String, QueueInfoList> queueInfoCache = CacheBuilder.newBuilder()
			.expireAfterWrite(3, TimeUnit.SECONDS).build(new CacheLoader<String, QueueInfoList>() {

				@Override
				public QueueInfoList load(String ignore) throws Exception {
					RabbitMQConfig cfg = ConfigFactory.create(RabbitMQConfig.class);
					String url = "http://" + cfg.hostname() + ":" + cfg.httpApiPort() + "/api/queues";
					String json = readUrlToString(url, cfg.username(), cfg.password());
					return GsonUtil.get().fromJson(json, QueueInfoList.class);
				}
			});

	public static QueueInfoList getQueueInfoList() {
		try {
			return queueInfoCache.get("");
		} catch (ExecutionException e) {
			logger.error("could not load cache info", e);
		}
		return null;
	}

	protected static String readUrlToString(String url, String user, String pass) throws IOException {
		URL connectUrl = new URL(url);
		URLConnection yc = connectUrl.openConnection();
		String userpass = user + ":" + pass;
		String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
		yc.setRequestProperty("Authorization", basicAuth);
		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		StringBuffer result = new StringBuffer();
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			result.append(inputLine);
		}
		in.close();
		return result.toString();
	}

	public static Channel getChannel() {
		if (_rabbitMQChannel == null || (_rabbitMQChannel.isPresent() && !_rabbitMQChannel.get().isOpen())) {
			init_rabbitMQChannel();
		}
		return _rabbitMQChannel != null && _rabbitMQChannel.isPresent() ? _rabbitMQChannel.get() : null;
	}

	private synchronized static void init_rabbitMQChannel() {
		if (_rabbitMQChannel == null || (_rabbitMQChannel.isPresent() && !_rabbitMQChannel.get().isOpen())) {
			try {
				Channel channel = getConnection().createChannel();
				channel.basicQos(1);
				_rabbitMQChannel = Optional.of(channel);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("could not connect to rabbit service", e);
			}
			if (_rabbitMQChannel == null) {
				_rabbitMQChannel = Optional.empty();
			}

		}

	}

	private static Connection getConnection() {
		if (_connectionOp == null || (!connectionDead && _connectionOp.isPresent() && !_connectionOp.get().isOpen())) {
			init_connectionOp();
		}
		return _connectionOp != null && _connectionOp.isPresent() ? _connectionOp.get() : null;
	}

	private synchronized static void init_connectionOp() {
		if (_connectionOp == null || (!connectionDead && _connectionOp.isPresent() && !_connectionOp.get().isOpen())) {
			RabbitMQConfig cfg = ConfigFactory.create(RabbitMQConfig.class);
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(cfg.hostname());
			factory.setPort(cfg.port());
			factory.setUsername(cfg.username());
			factory.setPassword(cfg.password());
			factory.setAutomaticRecoveryEnabled(true);
			factory.setRequestedHeartbeat(30);
			if (cfg.virtualHost() != null) {
				factory.setVirtualHost(cfg.virtualHost());
			}
			int attempt = 0;
			while (attempt < maxConnectionTry) {
				attempt++;
				try {
					Connection connection = factory.newConnection();
					_connectionOp = Optional.of(connection);
				} catch (IOException e) {
					e.printStackTrace();
					String msg = "could not connect to rabbit service";
					if (attempt < maxConnectionTry) {
						msg += ", retrying";
						try {
							Thread.sleep(attempt * 10 * 1000);
						} catch (InterruptedException e1) {
							logger.warn("slee interupt", e);
						}
					} else {
						msg += ", connection is dead. disabling rabbit";
						connectionDead = true;
					}
					logger.error(msg, e);
				}
			}
			if (_connectionOp == null) {
				_connectionOp = Optional.empty();
			}
		}
	}

	public static DeclareOk queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete,
			Map<String, Object> arguments) throws IOException {
		if (!declareMap.containsKey(queue)) {
			synchronized (declareMap) {
				if (!declareMap.containsKey(queue)) {
					declareMap.put(queue,
							RabbitMQ.getChannel().queueDeclare(queue, durable, exclusive, autoDelete, arguments));
				}
			}
		}
		return declareMap.get(queue);
	}
}
